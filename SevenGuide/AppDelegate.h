//
//  AppDelegate.h
//  SevenGuide
//
//  Created by Razvan Balazs on 30/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

