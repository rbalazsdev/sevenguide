//
//  SGProgramGuideViewController.h
//  SevenGuide
//
//  Created by Razvan Balazs on 30/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// Display a program guide fetched from the service.

#import <UIKit/UIKit.h>

@interface SGProgramGuideViewController : UITableViewController


@end

