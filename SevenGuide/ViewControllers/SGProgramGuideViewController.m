//
//  SGProgramGuideViewController.m
//  SevenGuide
//
//  Created by Razvan Balazs on 30/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//

#import "SGProgramGuideViewController.h"
#import "SGFetcherService.h"
#import "SGProgramInfoCell.h"

static NSString *kCellIdentifier = @"SGProgramInfoCellId";
static CGFloat kProgramCellHeight = 100;

@interface SGProgramGuideViewController () {
    NSMutableArray *_programs; // Keep it simple and store the programs in a list, will be udpdated as we get more items.
    NSUInteger _pageIndex; // Page index for the server pagnation.
    NSUInteger _programListTotalCount;
    BOOL _isFetchingData;
    UIActivityIndicatorView *_loadingIndicator; // Used as the background of the table view.
}

@end

@implementation SGProgramGuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[SGProgramInfoCell class] forCellReuseIdentifier:kCellIdentifier];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.title = @"Program Guide";
    
    _programs = [[NSMutableArray alloc] init];
    _pageIndex = 0;
    [self loadDataFromIndex:_pageIndex];
}

/// Load data from api. Index is the starting number for pagination.
- (void)loadDataFromIndex:(NSUInteger )index {
    _isFetchingData = YES;
    [self showLoading];
    __block SGProgramGuideViewController *weakSelf = self;
    [SGFetcherService requestProgramListFromIndex:index withCompletionBlock:^(NSUInteger programCount, NSArray *programList) {
        [self hideLoading];
        if (programList) {
            [_programs addObjectsFromArray:programList];
            [weakSelf.tableView reloadData];
            _pageIndex = _programs.count;
            
        }if (programCount) {
            _programListTotalCount = programCount;
        }
        else {
            // TODO display erorr.
        }
        _isFetchingData = NO;
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _programs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SGProgramInfoCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [cell setProgramInfo:[_programs objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate 

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kProgramCellHeight;
}

/// Check the third cell from the end, so we never see the spinner.
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _pageIndex-3 && _programListTotalCount > _pageIndex && !_isFetchingData ) {
        [self loadDataFromIndex:_pageIndex];
    }
}

#pragma mark - LoadingIndicator

/// Present loading spinner.
- (void)showLoading {
    if (!_loadingIndicator) {
        _loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.tableView.backgroundView = _loadingIndicator;
    }
    [_loadingIndicator startAnimating];
}

/// Hide the loading spinner.
- (void)hideLoading {
    [_loadingIndicator stopAnimating];
}

@end
