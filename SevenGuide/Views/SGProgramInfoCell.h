//
//  SGProgramInfoCell.h
//  SevenGuide
//
//  Created by Razvan Balazs on 30/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// Display information about a program.

#import <UIKit/UIKit.h>

@class SGProgram;

@interface SGProgramInfoCell : UITableViewCell

/// Update the program information to the cell.
- (void)setProgramInfo:(SGProgram *)program;
@end
