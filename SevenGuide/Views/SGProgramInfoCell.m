//
//  SGProgramInfoCell.m
//  SevenGuide
//
//  Created by Razvan Balazs on 30/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//

#import "SGProgramInfoCell.h"
#import "SGProgram.h"

static CGFloat kIndent = 8.;
static CGFloat kLabelHeight = 21.;

@interface SGProgramInfoCell () {
    UILabel *_nameLabel;
    UILabel *_startTimeLabel;
    UILabel *_endTimeLabel;
    UILabel *_channelNameLabel;
    UILabel *_ratingLabel;
    BOOL _constrainsExists;
}
@end

@implementation SGProgramInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:20];
        [self.contentView addSubview:_nameLabel];

        _startTimeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_startTimeLabel];

        _endTimeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_endTimeLabel];

        _channelNameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_channelNameLabel];

        _ratingLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_ratingLabel];
    }
    return self;
}

// TODO
- (void)layoutSubviews {
    [super layoutSubviews];
    int h = self.contentView.bounds.size.height;
    int w = self.contentView.bounds.size.width;
    
    _nameLabel.frame = CGRectMake(kIndent, kIndent,w-2*kIndent, kLabelHeight);

    _startTimeLabel.frame = CGRectMake(2*kIndent, 2*kIndent+kLabelHeight, (w-2*kIndent)/2, kLabelHeight);
    _endTimeLabel.frame = CGRectMake(3*kIndent +_startTimeLabel.intrinsicContentSize.width , 2*kIndent+kLabelHeight, _endTimeLabel.intrinsicContentSize.width, kLabelHeight);
    
    _channelNameLabel.frame = CGRectMake(w-kIndent - _channelNameLabel.intrinsicContentSize.width, h-kLabelHeight-kIndent, w -2*kIndent, kLabelHeight);
    _ratingLabel.frame =CGRectMake(2*kIndent, h-kLabelHeight-kIndent, _ratingLabel.intrinsicContentSize.width, kLabelHeight);
}

#pragma mark - Program info.

- (void)setProgramInfo:(SGProgram *)program {
    _nameLabel.text = program.name;
    _startTimeLabel.text = program.startTime;
    _endTimeLabel.text = [NSString stringWithFormat:@"- %@", program.endTime];
    _channelNameLabel.text = program.channelName;
    _ratingLabel.text = [NSString stringWithFormat:@"%@", program.rating];;
    
    [self setNeedsLayout];
}

@end
