//
//  SGProgram.h
//  SevenGuide
//
//  Created by Razvan Balazs on 30/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// Model class for a program.

#import <Foundation/Foundation.h>

@interface SGProgram : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *startTime; // Keep it as a string for the moment, convert it to NSDate later on.
@property (nonatomic, strong) NSString *endTime; // Keep it as a string for the moment, convert it to NSDate later on.
@property (nonatomic, strong) NSString *channelName;
@property (nonatomic, strong) NSString *rating;


/// Constructor from an dictionary retrieved by the service.
+ (instancetype)programFromDictionary:(NSDictionary *)dict;

@end
