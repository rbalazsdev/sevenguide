//
//  SGFetcherService.m
//  SevenGuide
//
//  Created by Razvan Balazs on 30/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// This class is responsible to fetch a list of programs.

#import "SGFetcherService.h"
#import "SGProgram.h"

static NSString *kIngestUrlString = @"http://www.whatsbeef.net/wabz/guide.php?start=";

@implementation SGFetcherService

+ (void)requestProgramListFromIndex:(NSUInteger )index withCompletionBlock:(FetchServiceCompletionBlock)completionBlock {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%u",kIngestUrlString,(unsigned int)index]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                           
                               if (!connectionError && data.length) {
                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                   NSMutableArray *output = [[NSMutableArray alloc] init];
                                 
                                   // TODO use a map with blocks to create the array directly.
                                   [json[@"results"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                       [output addObject:[SGProgram programFromDictionary:obj]];
                                   }];
                                   
                                   completionBlock([json[@"count"] integerValue],[NSArray arrayWithArray:output]);
                               }
                           
                           }];
    
}



@end
