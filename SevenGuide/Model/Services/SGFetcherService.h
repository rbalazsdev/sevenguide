//
//  SGFetcherService.h
//  SevenGuide
//
//  Created by Razvan Balazs on 30/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// This class is responsible to fetch a list of programs.

#import <Foundation/Foundation.h>

typedef void (^FetchServiceCompletionBlock) (NSUInteger programCount, NSArray *programList);

@interface SGFetcherService : NSObject

/// Request a list of programs from index on the API. Completion block will be called with nil if there was an error.
+ (void)requestProgramListFromIndex:(NSUInteger )index withCompletionBlock:(FetchServiceCompletionBlock)completionBlock;

@end
