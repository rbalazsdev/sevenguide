//
//  SGProgram.m
//  SevenGuide
//
//  Created by Razvan Balazs on 30/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//

#import "SGProgram.h"

@implementation SGProgram

+ (instancetype)programFromDictionary:(NSDictionary *)dict {
    SGProgram *program = [[SGProgram alloc] init];
    program.name = dict[@"name"];
    program.startTime = dict[@"start_time"];
    program.endTime = dict[@"end_time"];
    program.channelName = dict[@"channel"];
    program.rating = dict[@"rating"];
    return program;
}

@end
