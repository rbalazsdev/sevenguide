//
//  SevenGuideTests.m
//  SevenGuideTests
//
//  Created by Razvan Balazs on 30/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "SGProgram.h"

@interface SevenGuideTests : XCTestCase

@end

@implementation SevenGuideTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    NSDictionary *dict = @{ @"name": @"Adam Hills Tonight",
                            @"start_time": @"8:32pm",
                            @"end_time": @"9:31pm",
                            @"channel": @"ABC1",
                            @"rating": @"PG" };
    
    
    SGProgram *program = [SGProgram programFromDictionary:dict];
    XCTAssertEqual(program.name, @"Adam Hills Tonight");
    XCTAssertEqual(program.startTime, @"8:32pm");
    XCTAssertEqual(program.endTime, @"9:31pm");
    XCTAssertEqual(program.channelName, @"ABC1");
    XCTAssertEqual(program.rating, @"PG");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
